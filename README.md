# Projet Web Service

### Aurélien SOUTIL
### Usman MOHAMMAD

## Présentation du projet

Ce projet de réservation de trains a été construit avec docker afin de faciliter au maximum le déploiement des applications.
Il est divisé en 3 parties principales :

- Partie Client
    - Cette partie n'est malheureuseument pas finie car nous n'avons pas réussi à appeler les services SOAP en java proposés par la partie "Réservation".
    - Conceptuellement, c'est un site web qui permet de créer un compte sur le service de Réservation et d'y visualiser ses réservation de train.
    - Il est codé en java pour la partie backend avec des pages jsp pour la partie frontend.

- Partie Réservation
    - C'est un service SOAP qui permet aux utilisateurs s'y connectant de créer un compte client et de réserver des trajets de train, disponibles chez le Fournisseur
    - Il est codé en java et possède une base de données en PostgreSQL

- Partie Fournisseur
    - C'est un service REST qui permet aux utilisateurs s'y connectant de de créer des trajets, des réservations ou encore des stations chez le fournisseur
    - Conceptuellement, le site de réservation ne devrait pas pouvoir faire autre chose que des réservations, cependant cette API propose également une intégration en interne pour manipuler la base de données, il suffirait de masquer les services critiques pour qu'ils soient uniquement accessibles en interne.
    - Il est codé en java et possède une base de données en PostgreSQL

Un jeu complet et exaustif de tests POSTMAN est présent afin de pouvoir tester l'ensemble des services proposés par les 2 API.
Le service REST possède une documentation générée par OPEN API.

## Logiciels nécessaire pour build l'application

- make
- docker

## Commande pour lancer l'application

- make

## Comment explorer l'application

- Le site web client (non fontionnel) est exposé sur le port 8080
- Le service de Réservation (SOAP) est exposé sur le port 8082 (voir services POSTMAN)
- Le service Fournisseur (REST) est exposé sur le port 8081 (voir services POSTMAN)
- Le la documentationn de l'API REST est exposée sur le port 8083

Nous avons fais des raccourcis pour se connecter directement aux bases de données:
- make fournisseurdb : permet de se connecter à la BDD utilisée par l'API REST
- make reservationdb : permet de se connecter à la BDD utilisée par l'API SOAP

## Auto-évaluation

1.	Create REST Train Filtering service B -------------------------------------------- 5/6
2.	Create SOAP Train Booking service A ---------------------------------------------- 3/4
3.	Interaction between two services ------------------------------------------------- 3/4
4.	Test with Web service Client (instead of using Eclipse's Web service Explorer) --- 0/2
5.	Work with complex data type (class, table, etc.) --------------------------------- 2/2
6.	Work with database (in text file, xml, in mysql, etc.) --------------------------- 2/2
7.	Postman -------------------------------------------------------------------------- 2/2
8.	OpenAPI	-------------------------------------------------------------------------- 3/3
9.	BPMS ----------------------------------------------------------------------------- 2/5