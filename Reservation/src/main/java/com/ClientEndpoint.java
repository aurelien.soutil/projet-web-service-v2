package com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.baeldung.springsoap.gen.GetClientRequest;
import com.baeldung.springsoap.gen.GetClientResponse;
import com.baeldung.springsoap.gen.*;
import com.Usman_Aurelien.soap.model.Client;

@Endpoint
public class ClientEndpoint {

    private static final String NAMESPACE_URI = "http://www.baeldung.com/springsoap/gen";
    private final ClientRepository clientRepository;

    @Autowired
    public ClientEndpoint(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    private com.baeldung.springsoap.gen.Client convertToJaxb(Client clientEntity) {
        if (clientEntity == null) {
            return null;
        }
        com.baeldung.springsoap.gen.Client clientJaxb = new com.baeldung.springsoap.gen.Client();
        clientJaxb.setId(clientEntity.getClientId().intValue());
        clientJaxb.setUsername(clientEntity.getClientUsername());
        clientJaxb.setName(clientEntity.getClientNom());
        clientJaxb.setFirstname(clientEntity.getClientPrenom());
        clientJaxb.setPassword(clientEntity.getClientPassword());
        return clientJaxb;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getClientRequest")
    @ResponsePayload
    public GetClientResponse getClient(@RequestPayload GetClientRequest request) {
        GetClientResponse response = new GetClientResponse();
        Client clientEntity = clientRepository.findClient(request.getId());

        if (clientEntity != null) {
            response.setClient(convertToJaxb(clientEntity));
            System.out.println("Client: " + clientEntity.getClientId() + " " + clientEntity.getClientNom() + " " + clientEntity.getClientPrenom() + " " + clientEntity.getClientUsername() + " " + clientEntity.getClientPassword());
        } else {
            System.out.println("Client not found for ID: " + request.getId());
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getClientByUsernameRequest")
    @ResponsePayload
    public GetClientByUsernameResponse getClientByUsername(@RequestPayload GetClientByUsernameRequest request) {
        GetClientByUsernameResponse response = new GetClientByUsernameResponse();
        Client clientEntity = clientRepository.findClientByUsernamePassword(request.getUsername(), request.getPassword());

        if (clientEntity != null) {
            response.setClient(convertToJaxb(clientEntity));
            System.out.println("Client: " + clientEntity.getClientId() + " " + clientEntity.getClientNom() + " " + clientEntity.getClientPrenom() + " " + clientEntity.getClientUsername() + " " + clientEntity.getClientPassword());
        } else {
            System.out.println("Client not found for username: " + request.getUsername());
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addClientRequest")
    @ResponsePayload
    public AddClientResponse addClient(@RequestPayload AddClientRequest request) {
        AddClientResponse response = new AddClientResponse();
        Client clientEntity = new Client();
        clientEntity.setClientNom(request.getName());
        clientEntity.setClientPrenom(request.getFirstname());
        clientEntity.setClientUsername(request.getUsername());
        clientEntity.setClientPassword(request.getPassword());
        clientEntity = clientRepository.addClient(clientEntity);
        response.setClient(convertToJaxb(clientEntity));
        System.out.println("Client: " + clientEntity.getClientId() + " " + clientEntity.getClientNom() + " " + clientEntity.getClientPrenom() + " " + clientEntity.getClientUsername() + " " + clientEntity.getClientPassword());
        return response;
    }
}
