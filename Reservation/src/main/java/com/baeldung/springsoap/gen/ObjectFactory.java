//
// Ce fichier a été généré par Eclipse Implementation of JAXB, v3.0.0 
// Voir https://eclipse-ee4j.github.io/jaxb-ri 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2024.01.10 à 03:47:35 PM CET 
//


package com.baeldung.springsoap.gen;

import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.baeldung.springsoap.gen package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.baeldung.springsoap.gen
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetClientRequest }
     * 
     */
    public GetClientRequest createGetClientRequest() {
        return new GetClientRequest();
    }

    /**
     * Create an instance of {@link GetClientByUsernameRequest }
     * 
     */
    public GetClientByUsernameRequest createGetClientByUsernameRequest() {
        return new GetClientByUsernameRequest();
    }

    /**
     * Create an instance of {@link AddClientRequest }
     * 
     */
    public AddClientRequest createAddClientRequest() {
        return new AddClientRequest();
    }

    /**
     * Create an instance of {@link AddClientResponse }
     * 
     */
    public AddClientResponse createAddClientResponse() {
        return new AddClientResponse();
    }

    /**
     * Create an instance of {@link Client }
     * 
     */
    public Client createClient() {
        return new Client();
    }

    /**
     * Create an instance of {@link GetClientResponse }
     * 
     */
    public GetClientResponse createGetClientResponse() {
        return new GetClientResponse();
    }

    /**
     * Create an instance of {@link GetClientByUsernameResponse }
     * 
     */
    public GetClientByUsernameResponse createGetClientByUsernameResponse() {
        return new GetClientByUsernameResponse();
    }

    /**
     * Create an instance of {@link GetReservationRequest }
     * 
     */
    public GetReservationRequest createGetReservationRequest() {
        return new GetReservationRequest();
    }

    /**
     * Create an instance of {@link GetReservationResponse }
     * 
     */
    public GetReservationResponse createGetReservationResponse() {
        return new GetReservationResponse();
    }

    /**
     * Create an instance of {@link Reservation }
     * 
     */
    public Reservation createReservation() {
        return new Reservation();
    }

    /**
     * Create an instance of {@link GetAllReservationsRequest }
     * 
     */
    public GetAllReservationsRequest createGetAllReservationsRequest() {
        return new GetAllReservationsRequest();
    }

    /**
     * Create an instance of {@link GetAllReservationsResponse }
     * 
     */
    public GetAllReservationsResponse createGetAllReservationsResponse() {
        return new GetAllReservationsResponse();
    }

    /**
     * Create an instance of {@link GetAllReservationsSurRequest }
     * 
     */
    public GetAllReservationsSurRequest createGetAllReservationsSurRequest() {
        return new GetAllReservationsSurRequest();
    }

    /**
     * Create an instance of {@link GetAllReservationsSurResponse }
     * 
     */
    public GetAllReservationsSurResponse createGetAllReservationsSurResponse() {
        return new GetAllReservationsSurResponse();
    }

    /**
     * Create an instance of {@link ReservationAll }
     * 
     */
    public ReservationAll createReservationAll() {
        return new ReservationAll();
    }

    /**
     * Create an instance of {@link AddReservationRequest }
     * 
     */
    public AddReservationRequest createAddReservationRequest() {
        return new AddReservationRequest();
    }

    /**
     * Create an instance of {@link AddReservationResponse }
     * 
     */
    public AddReservationResponse createAddReservationResponse() {
        return new AddReservationResponse();
    }

}
