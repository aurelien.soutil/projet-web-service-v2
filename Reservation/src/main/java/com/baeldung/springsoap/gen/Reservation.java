//
// Ce fichier a été généré par Eclipse Implementation of JAXB, v3.0.0 
// Voir https://eclipse-ee4j.github.io/jaxb-ri 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2024.01.10 à 03:47:35 PM CET 
//


package com.baeldung.springsoap.gen;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour reservation complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="reservation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="extid" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="clientid" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservation", propOrder = {
    "id",
    "extid",
    "clientid"
})
public class Reservation {

    protected int id;
    protected int extid;
    protected int clientid;

    /**
     * Obtient la valeur de la propriété id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété extid.
     * 
     */
    public int getExtid() {
        return extid;
    }

    /**
     * Définit la valeur de la propriété extid.
     * 
     */
    public void setExtid(int value) {
        this.extid = value;
    }

    /**
     * Obtient la valeur de la propriété clientid.
     * 
     */
    public int getClientid() {
        return clientid;
    }

    /**
     * Définit la valeur de la propriété clientid.
     * 
     */
    public void setClientid(int value) {
        this.clientid = value;
    }

}
