//
// Ce fichier a été généré par Eclipse Implementation of JAXB, v3.0.0 
// Voir https://eclipse-ee4j.github.io/jaxb-ri 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2024.01.10 à 03:47:35 PM CET 
//


package com.baeldung.springsoap.gen;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="clientid" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="trajetid" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="resaClasse" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "clientid",
    "trajetid",
    "resaClasse"
})
@XmlRootElement(name = "addReservationRequest")
public class AddReservationRequest {

    protected int clientid;
    protected int trajetid;
    @XmlElement(required = true)
    protected String resaClasse;

    /**
     * Obtient la valeur de la propriété clientid.
     * 
     */
    public int getClientid() {
        return clientid;
    }

    /**
     * Définit la valeur de la propriété clientid.
     * 
     */
    public void setClientid(int value) {
        this.clientid = value;
    }

    /**
     * Obtient la valeur de la propriété trajetid.
     * 
     */
    public int getTrajetid() {
        return trajetid;
    }

    /**
     * Définit la valeur de la propriété trajetid.
     * 
     */
    public void setTrajetid(int value) {
        this.trajetid = value;
    }

    /**
     * Obtient la valeur de la propriété resaClasse.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResaClasse() {
        return resaClasse;
    }

    /**
     * Définit la valeur de la propriété resaClasse.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResaClasse(String value) {
        this.resaClasse = value;
    }

}
