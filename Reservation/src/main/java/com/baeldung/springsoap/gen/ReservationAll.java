//
// Ce fichier a été généré par Eclipse Implementation of JAXB, v3.0.0 
// Voir https://eclipse-ee4j.github.io/jaxb-ri 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2024.01.10 à 03:47:35 PM CET 
//


package com.baeldung.springsoap.gen;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour reservationAll complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="reservationAll"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="resaId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="trajetId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="resaNomPassager" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="resaPrenomPassager" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="resaClasse" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reservationAll", propOrder = {
    "resaId",
    "trajetId",
    "resaNomPassager",
    "resaPrenomPassager",
    "resaClasse"
})
public class ReservationAll {

    protected int resaId;
    protected int trajetId;
    @XmlElement(required = true)
    protected String resaNomPassager;
    @XmlElement(required = true)
    protected String resaPrenomPassager;
    @XmlElement(required = true)
    protected String resaClasse;

    /**
     * Obtient la valeur de la propriété resaId.
     * 
     */
    public int getResaId() {
        return resaId;
    }

    /**
     * Définit la valeur de la propriété resaId.
     * 
     */
    public void setResaId(int value) {
        this.resaId = value;
    }

    /**
     * Obtient la valeur de la propriété trajetId.
     * 
     */
    public int getTrajetId() {
        return trajetId;
    }

    /**
     * Définit la valeur de la propriété trajetId.
     * 
     */
    public void setTrajetId(int value) {
        this.trajetId = value;
    }

    /**
     * Obtient la valeur de la propriété resaNomPassager.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResaNomPassager() {
        return resaNomPassager;
    }

    /**
     * Définit la valeur de la propriété resaNomPassager.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResaNomPassager(String value) {
        this.resaNomPassager = value;
    }

    /**
     * Obtient la valeur de la propriété resaPrenomPassager.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResaPrenomPassager() {
        return resaPrenomPassager;
    }

    /**
     * Définit la valeur de la propriété resaPrenomPassager.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResaPrenomPassager(String value) {
        this.resaPrenomPassager = value;
    }

    /**
     * Obtient la valeur de la propriété resaClasse.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResaClasse() {
        return resaClasse;
    }

    /**
     * Définit la valeur de la propriété resaClasse.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResaClasse(String value) {
        this.resaClasse = value;
    }

}
