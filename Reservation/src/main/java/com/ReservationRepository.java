package com;

import com.Usman_Aurelien.soap.model.Reservation;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

@Component
public class ReservationRepository {

    private static final Map<Integer, Reservation> reservations = new HashMap<>();

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    @Transactional
    public void initData() {
        List<Reservation> clientList = entityManager.createQuery("SELECT c FROM Reservation c", Reservation.class).getResultList();
        for (Reservation client : clientList) {
            reservations.put(client.getResaId(), client);
        }
    }

    // je retourne une reservation en fonction de son id et celui du client
    public Reservation findReservationIdClientId(int idReservation, int idClient) {
        for (Reservation reservation : reservations.values()) {
            if (reservation.getResaId() == idReservation && reservation.getClientId() == idClient) {
                return reservation;
            }
        }
        return null;
        
    }

    public List<Reservation> findReservationIdClient(int idClient) {
        List<Reservation> reservationList = entityManager.createQuery("SELECT c FROM Reservation c WHERE c.clientId = :idClient", Reservation.class).setParameter("idClient", idClient).getResultList();
        return reservationList;
    }

    @Transactional
    public Reservation addReservation(Reservation reservation) {
        entityManager.persist(reservation);
        reservations.put(reservation.getResaId(), reservation);
        return reservation;
    }
}

