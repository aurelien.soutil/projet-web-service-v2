package com;

import com.Usman_Aurelien.soap.model.Client;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

@Component
public class ClientRepository {

    private static final Map<Integer, Client> clients = new HashMap<>();

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    @Transactional
    public void initData() {
        List<Client> clientList = entityManager.createQuery("SELECT c FROM Client c", Client.class).getResultList();
        for (Client client : clientList) {
            clients.put(client.getClientId().intValue(), client);
        }
    }

    public Client findClient(int id) {
        return clients.get(id);
    }

    public Client findClientByUsernamePassword(String username, String password) {
        for (Client client : clients.values()) {
            if (client.getClientUsername().equals(username) && client.getClientPassword().equals(password)) {
                return client;
            }
        }
        return null;
    }

    @Transactional
    public Client addClient(Client client) {
        entityManager.persist(client);
        clients.put(client.getClientId().intValue(), client); // Ajoutez également au cache local
        return client;
    }
}

