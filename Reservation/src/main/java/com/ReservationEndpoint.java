package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.baeldung.springsoap.gen.*;
import com.Usman_Aurelien.soap.model.Reservation;

@Endpoint
public class ReservationEndpoint {

    private static final String NAMESPACE_URI = "http://www.baeldung.com/springsoap/gen";
    private final ReservationRepository reservationRepository;
    private static final String REST_RESERVATIONS_ENDPOINT = "http://host.docker.internal:8081/api/reservations/";
    private final RestTemplate restTemplate;
    private final ClientRepository clientRepository;

    @Autowired
    public ReservationEndpoint(ReservationRepository reservationRepository, RestTemplate restTemplate, ClientRepository clientRepository) {
        this.reservationRepository = reservationRepository;
        this.clientRepository = clientRepository;
        this.restTemplate = restTemplate;
    }

    private com.baeldung.springsoap.gen.Reservation convertToJaxb(Reservation reservationEntity) {
        if (reservationEntity == null) {
            return null;
        }
        com.baeldung.springsoap.gen.Reservation reservationJaxb = new com.baeldung.springsoap.gen.Reservation();
        reservationJaxb.setId(reservationEntity.getResaId());
        reservationJaxb.setClientid(reservationEntity.getClientId());
        reservationJaxb.setExtid(reservationEntity.getResaExtId());
        return reservationJaxb;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getReservationRequest")
    @ResponsePayload
    public GetReservationResponse getReservation(@RequestPayload GetReservationRequest request) {
        GetReservationResponse response = new GetReservationResponse();
        Reservation reservationEntity = reservationRepository.findReservationIdClientId(request.getId(), request.getClientid());

        if (reservationEntity != null) {
            response.setReservation(convertToJaxb(reservationEntity));
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllReservationsRequest")
    @ResponsePayload
    public GetAllReservationsResponse getAllReservations(@RequestPayload GetAllReservationsRequest request) {
        GetAllReservationsResponse response = new GetAllReservationsResponse();
        for (Reservation reservationEntity : reservationRepository.findReservationIdClient(request.getClientid())) {
            response.getReservation().add(convertToJaxb(reservationEntity));
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addReservationRequest")
    @ResponsePayload
    public AddReservationResponse addReservation(@RequestPayload AddReservationRequest request) {
        AddReservationResponse response = new AddReservationResponse();
        
        int trajetId = request.getTrajetid();

        int clientId = request.getClientid();

        com.Usman_Aurelien.soap.model.Client clientEntity = clientRepository.findClient(clientId);

        String resaNomPassager = clientEntity.getClientNom();
        String resaPrenomPassager = clientEntity.getClientPrenom();
        String resaClasse = request.getResaClasse();

        // Insertion réservatoin coté fourrnisseur
        insertReservationREST(trajetId, resaNomPassager, resaPrenomPassager, resaClasse);

        // Recup id créé
        int extId = getLastReservationId();

        // Save en base et renvoi de l'élément créé
        Reservation res = new Reservation();
        res.setClientId(clientId);
        res.setResaExtId(extId);

        res = reservationRepository.addReservation(res);

        response.setReservation(convertToJaxb(res));
        return response;
    }

    public void insertReservationREST(int trajetId, String resaNomPassager, String resaPrenomPassager, String resaClasse) {
        String requestBody = "{ \"trajetId\": \"" + trajetId + "\", \"resaNomPassager\": \"" + resaNomPassager +
                         "\", \"resaPrenomPassager\": \"" + resaPrenomPassager + "\", \"resaClasse\": \"" + resaClasse + "\" }";


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        // Encapsulation de l'objet JSON dans un HttpEntity avec les en-têtes définis
        HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);

        // Appel de l'API REST avec RestTemplate
        ResponseEntity<List<Reservation>> response = restTemplate.exchange(
            REST_RESERVATIONS_ENDPOINT,
            HttpMethod.POST,
            requestEntity,
            new ParameterizedTypeReference<List<Reservation>>() {}
        );

    }

    private final class ReservationComparator implements Comparator<Reservation> {
        public int compare(Reservation a, Reservation b) {
            if (a.getResaId() > b.getResaId())
                return -1; // highest value first
            if (a.getResaId() == b.getResaId())
                return 0;
            return 1;
        }
    }

    public Integer getLastReservationId() {
        ResponseEntity<List<Reservation>> response = restTemplate.exchange(
            REST_RESERVATIONS_ENDPOINT,
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Reservation>>() {});

        

        List<Reservation> reservations = response.getBody();
        reservations.sort(new ReservationComparator());
        
        return reservations.isEmpty() ? null : reservations.get(0).getResaId();
    }

    public ReservationAll getReservationDetails(int resaId) {
        ResponseEntity<ReservationAll> response = restTemplate.exchange(
                REST_RESERVATIONS_ENDPOINT + resaId, // URL pour une réservation spécifique
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ReservationAll>() {});
        
        return response.getBody();
    }
    
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllReservationsSurRequest")
    @ResponsePayload
    public GetAllReservationsSurResponse getAllReservationsSur(@RequestPayload GetAllReservationsSurRequest request) {
        GetAllReservationsSurResponse response = new GetAllReservationsSurResponse();
    
        // Récupère tous les extId des réservations pour l'ID client spécifié
        List<Reservation> reservations = reservationRepository.findReservationIdClient(request.getClientid());
    
        for (Reservation reservation : reservations) {
            // Récupère les détails de chaque réservation via REST
            ReservationAll reservationAll = getReservationDetails(reservation.getResaExtId());
            response.getReservationAll().add(reservationAll);
        }
    
        return response;
    }
    
}
