package com.Usman_Aurelien.soap.model;

import jakarta.persistence.*;

@Entity
@Table(name = "soap_reservation")
public class Reservation {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "resaid")
    private int resaId;

    @Column(name = "resaextid", nullable = false)
    private int resaExtId;

    @Column(name = "clientid", nullable = false)
    private int clientId;

    public Reservation() {
    }
    
    public int getResaId() {
        return resaId;
    }

    public int getResaExtId() {
        return resaExtId;
    }

    public void setResaExtId(int resaExtId) {
        this.resaExtId = resaExtId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientid) {
        this.clientId = clientid;
    }
}
