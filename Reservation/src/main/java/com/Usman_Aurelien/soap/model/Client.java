package com.Usman_Aurelien.soap.model;

import jakarta.persistence.*;

@Entity
@Table(name = "soap_client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "clientid")
    private Long clientId;

    @Column(name = "clientnom", nullable = false, length = 50)
    private String clientNom;

    @Column(name = "clientprenom", nullable = false, length = 50)
    private String clientPrenom;

    @Column(name = "clientusername", nullable = false, length = 50)
    private String clientUsername;

    @Column(name = "clientpassword", nullable = false, length = 50)
    private String clientPassword;

    public Client() {
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getClientNom() {
        return clientNom;
    }

    public void setClientNom(String clientNom) {
        this.clientNom = clientNom;
    }

    public String getClientPrenom() {
        return clientPrenom;
    }

    public void setClientPrenom(String clientPrenom) {
        this.clientPrenom = clientPrenom;
    }

    public String getClientUsername() {
        return clientUsername;
    }

    public void setClientUsername(String clientUsername) {
        this.clientUsername = clientUsername;
    }

    public String getClientPassword() {
        return clientPassword;
    }

    public void setClientPassword(String clientPassword) {
        this.clientPassword = clientPassword;
    }
}
