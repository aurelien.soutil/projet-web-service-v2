-- Création de toutes les tables pour train booking

CREATE TABLE SOAP_Client (
    ClientId SERIAL PRIMARY KEY,
    ClientNom VARCHAR(50) NOT NULL,
    ClientPrenom VARCHAR(50) NOT NULL,
    ClientUsername VARCHAR(50) NOT NULL,
    ClientPassword VARCHAR(50) NOT NULL
);

CREATE TABLE SOAP_Reservation (
    ResaId SERIAL PRIMARY KEY,
    ResaExtId INTEGER NOT NULL,
    ClientId INTEGER NOT NULL,
    FOREIGN KEY (ClientId) REFERENCES SOAP_Client (ClientId)
);

INSERT INTO SOAP_Client (ClientNom, ClientPrenom, ClientUsername, ClientPassword)
VALUES 
    ('Dupont', 'Jean', 'jdupont', 'mdp123'),
    ('Smith', 'Emma', 'esmith', 'motdepasse1'),
    ('Garcia', 'Luis', 'lgarcia', '12345678'),
    ('Johnson', 'Sophie', 'sjohnson', 'azerty'),
    ('Chen', 'Wei', 'wchen', 'mdp456');