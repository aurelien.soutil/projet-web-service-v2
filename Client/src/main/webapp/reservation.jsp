<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="objects.Reservation" %>

<%
    String clientNom = (String) request.getAttribute("clientNom");
    String clientPrenom = (String) request.getAttribute("clientPrenom");
%>

<!DOCTYPE html>

<html>
<head>
    <title>RésaTrain</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

    <div class="container">
        <!-- Réservation -->
        <div class="reservation-section">
            <% if (clientNom != null && clientPrenom != null) { %>
                <h1>BIENVENUE <%= clientPrenom %> <%= clientNom %></h1>
            <% } %>
            <h2>Vos réservations</h2>

        <% List<Reservation> reservations = (List<Reservation>) request.getAttribute("reservations"); %>
        <% if (reservations != null && !reservations.isEmpty()) { %>
            <ul>
                <% for (Reservation reservation : reservations) { %>
                    <li>
                        Départ : <%= reservation.getStationDepart() %><br>
                        Arrivée : <%= reservation.getStationArrivee() %><br>
                        Date de départ : <%= reservation.getDateDepart() %><br>
                        Date d'arrivée : <%= reservation.getDateArrivee() %><br>
                        Classe : <%= reservation.getClasse() %>
                    </li>
                <% } %>
            </ul>
        <% } else { %>
            <p>Aucune réservation trouvée.</p>
        <% } %>

        </div>

        <!-- Recherche -->
        <div class="search-section">
            <h2>Rechercher un train</h2>
            <form method="POST" action="SearchServlet">
                <label for="depart">Gare de départ:</label>
                <input type="text" id="depart" name="depart"><br><br>

                <label for="arrivee">Gare d'arrivée:</label>
                <input type="text" id="arrivee" name="arrivee"><br><br>

                <label for="dateDepart">Date de départ:</label>
                <input type="date" id="dateDepart" name="dateDepart"><br><br>

                <label for="dateArrivee">Date d'arrivée':</label>
                <input type="date" id="dateArrivee" name="dateArrivee"><br><br>

                <input type="submit" value="Rechercher">
            </form>
        </div>

        <div class="search-result">
            <% List<Reservation> searchResult = (List<Reservation>) request.getAttribute("searchResult"); %>
            <% if (searchResult != null && !searchResult.isEmpty()) { %>
                <h2>Résultats de recherche</h2>
                <ul>
                    <% for (Reservation reservation : searchResult) { %>
                        <li>
                            Départ : <%= reservation.getStationDepart() %><br>
                            Arrivée : <%= reservation.getStationArrivee() %><br>
                            Date de départ : <%= reservation.getDateDepart() %><br>
                            Date d'arrivée : <%= reservation.getDateArrivee() %><br>
                            Classe : <%= reservation.getClasse() %>
                        </li>
                    <% } %>
                </ul>
            <% } else { %>
                <p>Aucun résultat trouvé.</p>
            <% } %>
        </div>
    </div>

</body>
</html>
