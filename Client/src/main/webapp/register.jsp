<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>

<html>
    <head>
        <title>RésaTrain - Inscription</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    
    <body>
        <div class="login-container">
            <h2>RésaTrain - Inscription</h2>

            <% String errorMessage = (String) request.getAttribute("errorMessage"); %>
            <% if (errorMessage != null && !errorMessage.isEmpty()) { %>
                <div class="error-message">
                    <p><strong>Formulaire incorrect :</strong> <%= errorMessage %></p>
                </div>
            <% } %>

            <form action="RegisterServlet" method="post">
                <div class="input-group">
                    <label for="username">Nom d'utilisateur :</label>
                    <input type="text" id="username" name="username" required>
                </div>

                <div class="input-group">
                    <label for="name">Nom :</label>
                    <input type="text" id="name" name="name" required>
                </div>

                <div class="input-group">
                    <label for="firstName">Prénom :</label>
                    <input type="text" id="firstName" name="firstName" required>
                </div>

                <div class="input-group">
                    <label for="password">Mot de passe :</label>
                    <input type="password" id="password" name="password" required>
                </div>
                <input type="submit" value="S'inscrire">
            </form>

            <p>
                <a href="login.jsp">Retour à la page de connexion</a>
            </p>

        </div>
    </body>
</html>
