<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>

<html>
    <head>
        <title>RésaTrain - Connexion</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    
    <body>
        <div class="login-container">
            <h2>RésaTrain - Connexion</h2>

            <% String errorMessage = (String) request.getAttribute("errorMessage"); %>
            <% if (errorMessage != null && !errorMessage.isEmpty()) { %>
                <div class="error-message">
                    <p><strong>Identifiants incorrects :</strong> <%= errorMessage %></p>
                </div>
            <% } %>

            <form action="LoginServlet" method="post">
                <div class="input-group">
                    <label for="username">Nom d'utilisateur :</label>
                    <input type="text" id="username" name="username" required>
                </div>
                <div class="input-group">
                    <label for="password">Mot de passe :</label>
                    <input type="password" id="password" name="password" required>
                </div>
                <input type="submit" value="Se connecter">
            </form>
            <p>Pas de compte ? <a href="register.jsp">Inscrivez-vous !</a></p>
        </div>
    </body>
</html>
