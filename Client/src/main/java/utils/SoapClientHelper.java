package utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import objects.Client;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class SoapClientHelper {

    public static Client getClientFromSoapService(String username, String password) throws IOException, ParserConfigurationException, SAXException {
        String soapRequest = buildSoapRequest(username, password);
        String soapResponse = sendSoapRequest(soapRequest, "http://host.docker.internal:8082/ws/client.wsdl");
        return parseSoapResponse(soapResponse);
    }

    private static String buildSoapRequest(String username, String password) {
        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                + "xmlns:gs=\"http://www.baeldung.com/springsoap/gen\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<gs:getClientByUsernameRequest>"
                + "<gs:username>" + username + "</gs:username>"
                + "<gs:password>" + password + "</gs:password>"
                + "</gs:getClientByUsernameRequest>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";
    }

    private static String sendSoapRequest(String soapRequest, String url) throws IOException {
        URL endpoint = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) endpoint.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "text/xml");
        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(soapRequest.getBytes());
        outputStream.flush();

        if (connection.getResponseCode() == 200) {
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;
            while ((length = connection.getInputStream().read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            return result.toString("UTF-8");
        } else {
            throw new IOException("HTTP error code : " + connection.getResponseCode());
        }
    }

    private static Client parseSoapResponse(String soapResponse) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        ByteArrayInputStream input = new ByteArrayInputStream(soapResponse.getBytes("UTF-8"));
        Document doc = builder.parse(input);
        doc.getDocumentElement().normalize();
        String clientIdStr = doc.getElementsByTagName("ns2:id").item(0).getTextContent();
        int clientId = Integer.parseInt(clientIdStr);
        
        return new Client(clientId);
    }

    public static int createClientInSoapService(String username, String name, String firstName, String password) throws Exception {
        String soapRequest = buildCreateClientSoapRequest(username, name, firstName, password);
        String soapResponse = sendSoapRequest(soapRequest, "http://host.docker.internal:8082/ws/client.wsdl");
        return parseCreateClientSoapResponse(soapResponse);
    }

    private static String buildCreateClientSoapRequest(String username, String name, String firstName, String password) {
        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                + "xmlns:gs=\"http://www.baeldung.com/springsoap/gen\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<gs:addClientRequest>"
                + "<gs:username>" + username + "</gs:username>"
                + "<gs:name>" + name + "</gs:name>"
                + "<gs:firstname>" + firstName + "</gs:firstname>"
                + "<gs:password>" + password + "</gs:password>"
                + "</gs:addClientRequest>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";
    }

    private static int parseCreateClientSoapResponse(String soapResponse) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        ByteArrayInputStream input = new ByteArrayInputStream(soapResponse.getBytes("UTF-8"));
        Document doc = builder.parse(input);
        doc.getDocumentElement().normalize();

        // Check if the response contains the expected elements
        NodeList clientList = doc.getElementsByTagName("ns2:client");
        if (clientList.getLength() > 0) {
            Element clientElement = (Element) clientList.item(0);
            return Integer.parseInt(clientElement.getElementsByTagName("ns2:id").item(0).getTextContent());
        } else {
        // Handle the case where the client is not created properly or the response is not as expected
        throw new RuntimeException("Client not created or unexpected SOAP response format.");
        }
        }

}
