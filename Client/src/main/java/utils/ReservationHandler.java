package utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.*;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;



public class ReservationHandler {

    public static String doSOAPPost() throws IOException{
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = 
        new HttpPost("http://host.docker.internal:8082/ws/client.wsdl");
        httpPost.addHeader("content-type", "text/xml");

        StringBuffer buffer = new StringBuffer();
        buffer.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"");
        buffer.append("xmlns:gs=\"http://www.baeldung.com/springsoap/gen\">");
        buffer.append(" <soapenv:Header/>");
        buffer.append("<soapenv:Body>");
        buffer.append("<gs:getClientRequest>");
        buffer.append("<gs:id>1</gs:id>");
        buffer.append("</gs:getClientRequest>");
        buffer.append("</soapenv:Body>");
        buffer.append("</soapenv:Envelope>");

        StringEntity lEntity = new StringEntity(buffer.toString());
        httpPost.setEntity(lEntity);

        HttpResponse lHttpResponse = httpClient.execute(httpPost);

        if (lHttpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK){
            throw new RuntimeException("HTTP problems posting method " + 
            lHttpResponse.getStatusLine().getReasonPhrase());
        }
        return EntityUtils.toString(lHttpResponse.getEntity());

    }

}