package servlets;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import objects.Reservation;

import java.util.Date;
import java.util.List;

public class SearchServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String stationDepart = request.getParameter("stationDepart");
        String stationArrivee = request.getParameter("stationArrivee");
        String classe = request.getParameter("classe");

        String stringDateDepart = request.getParameter("dateDepart");
        String stringDateArrivee = request.getParameter("dateArrivee");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date dateDepart = null;
        Date dateArrivee = null;

        try {
            dateDepart = dateFormat.parse(stringDateDepart);
            dateArrivee = dateFormat.parse(stringDateArrivee);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<Reservation> reservations = Reservation.getAvailableReservation(stationDepart, stationArrivee, dateDepart, dateArrivee, classe);

        request.setAttribute("searchResult", reservations);
        RequestDispatcher dispatcher = request.getRequestDispatcher("reservation.jsp");
        dispatcher.forward(request, response);
    }
}