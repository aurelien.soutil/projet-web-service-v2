package servlets;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import objects.Client;
import utils.ReservationHandler;

import java.io.*;

public class LoginServlet extends HttpServlet {
    // Temps de validité des cookies en secondes
    private final int cookieDuration = 60 * 2 * 60; // 2h

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        Client c = new Client(username, password);

        if (c.getClientId() != 0) {
            // Ajout d'un cookie de connexion (très succint)
            Cookie clientCookie = new Cookie("ClientId", String.valueOf(c.getClientId()));
            clientCookie.setMaxAge(cookieDuration);
            response.addCookie(clientCookie);
            response.sendRedirect("reservation.jsp");
        } else {
            String errorMessage = "Nom d'utilisateur ou mot de passe incorrects.";
            request.setAttribute("errorMessage", errorMessage);
            RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
            dispatcher.forward(request, response);
        }
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // response.sendRedirect("login.jsp");
        
        PrintWriter out = response.getWriter();
        out.write(ReservationHandler.doSOAPPost());
        // out.write("<h3>" + client.getId() +"</h3>");
        // out.write("<h3>" + client.getFirstname() +"</h3>");
        // out.write("<h3>" + client.getName() +"</h3>");
        // out.write("<h3>" + client.getUsername() +"</h3>");
        // out.write("<h3>" + client.getPassword() +"</h3>");
        out.close();
    }

}