package servlets;

import java.io.IOException;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import objects.*;

public class RegisterServlet extends HttpServlet {
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String name = request.getParameter("name");
        String firstName = request.getParameter("firstName");
        String password = request.getParameter("password");

        if (verifyForm(username, name, firstName, password)) {
            try {
                Client c = new Client();
                c.createClient(username, name, firstName, password);
                response.sendRedirect("login.jsp");
            } catch (Exception e) {
                e.printStackTrace(); // Log pour le débogage
                String errorMessage = "Erreur lors de la création du client. Veuillez réessayer.";
                request.setAttribute("errorMessage", errorMessage);
                RequestDispatcher dispatcher = request.getRequestDispatcher("register.jsp");
                dispatcher.forward(request, response);
            }
        } else {
            String errorMessage = "Veuillez remplir tous les champs.";
            request.setAttribute("errorMessage", errorMessage);
            RequestDispatcher dispatcher = request.getRequestDispatcher("register.jsp");
            dispatcher.forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("register.jsp");
    }

    // Pas beaucoup de vérifications mais ça fera l'affaire
    private boolean verifyForm(String username, String name, String firstName, String password) {
        return username != "" && name != "" && firstName != "" && password != "";
    }

}
