package objects;

import java.util.Date;
import java.util.List;

public class Reservation {
    
    private int ResaId;
    private String StationDepart;
    private String StationArrivee;
    private Date DateDepart;
    private Date DateArrivee;
    private String Classe;

    // Constructeurs**********************************************************

    public Reservation(int ResaId) {
        initVariables(ResaId);
    }


    // Fonction internes ******************************************************

    private void initVariables(int ResaId) {
        if (ResaId == 0) {
            this.ResaId = 0;
            this.StationDepart = ""; 
            this.StationArrivee = ""; 
            this.DateDepart = null; 
            this.DateArrivee = null; 
            this.Classe = ""; 
            
        }else {
            // TODO Récupérations valeurs via api SOAP

        }
    }


    // Fonctions publiques ****************************************************

    public int getResaId() {
        return ResaId;
    }

    public String getStationDepart() {
        return StationDepart;
    }

    public String getStationArrivee() {
        return StationArrivee;
    }

    public Date getDateDepart() {
        return DateDepart;
    }

    public Date getDateArrivee() {
        return DateArrivee;
    }

    public String getClasse() {
        return Classe;
    }


    // Fonctions statiques ****************************************************

    public static List<Reservation> getAvailableReservation(String StationDepart, String StationArrivee,
                                                            Date DateDepart, Date DateArrivee, String Classe) {
        // TODO récupération Reservations disponible via api SOAP
        return null;
    }

}
