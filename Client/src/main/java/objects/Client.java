package objects;

import java.util.List;

import jakarta.servlet.http.*;

public class Client {

    private int ClientId;
    private String ClientNom;
    private String ClientPrenom;
    private String ClientUsername;
    private String ClientPassword;


    // Constructeurs**********************************************************
    
    public Client() {
        initVariables(0);
    }

    public Client(int ClientId) {
        int verifiedClientId = verifyClientId(ClientId);
        initVariables(verifiedClientId);
    }

    public Client(String ClientUsername, String ClientPassword) {
        int ClientId = connectClient(ClientUsername, ClientPassword);
        initVariables(ClientId);
    }

    public Client(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
    
        boolean found = false;

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("ClientId")) {
                    int verifiedClientId = verifyClientId(cookie.getValue());
                    initVariables(verifiedClientId);
                    found = true;
                }
            }
        }

        if (!found) {
            initVariables(0);
        }
    }


    // Fonction internes ******************************************************

    private void initVariables(int ClientId) {
        if (ClientId == 0) {
            this.ClientId = 0;
            this.ClientNom = ""; 
            this.ClientPrenom = ""; 
            this.ClientUsername = ""; 
            this.ClientPassword = ""; 
        }else {
            // TODO Récupérations valeurs via api SOAP

        }
    }

    // Retourne le ClientId si la string est correcte
    // et que le ClientId existe dans le site de réservation, 0 sinon
    private int verifyClientId(String ClientId) {
        int id;

        // On vérifie que la chaine soit un entier
        try{
            id = Integer.parseInt(ClientId);
        } catch (NumberFormatException e) {
            return 0;
        }

        // Si oui alors on appelle la fonction de vérification sur les entiers
        return verifyClientId(id);
    }

    // Retourne le ClientId s'il existe dans le site de réservation, 0 sinon
    private int verifyClientId(int ClientId) {
        // TODO vérification id via api SOAP
        return 0;
    }

    private int connectClient(String ClientUsername, String ClientPassword) {
        // TODO récupération id Client
        return 0;
    }


    // Fonctions publiques ****************************************************

    public int getClientId() {
        return ClientId;
    }

    public String getClientNom() {
        return ClientNom;
    }


    public String getClientPrenom() {
        return ClientPrenom;
    }

    public List<Reservation> getAllReservation() {
        // TODO Récupération de toutes les réservations d'un client
        return null;
    }

    @Override
    public String toString() {
        return ClientUsername;
    }

    public void createClient(String username, String name, String firstName, String password) {
        // TODO Création d'un client
    }

}
