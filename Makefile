all:
	docker-compose up build_client build_reservation build_fournisseur
	docker-compose up client reservation reservation_db fournisseur fournisseur_db swagger-ui

build:
	docker-compose up build_client build_reservation build_fournisseur

run:
	docker-compose up client reservation reservation_db fournisseur fournisseur_db swagger-ui

swaggerapi:
	docker-compose up swagger-ui

fournisseurdb:
	docker exec -it fournisseur_db psql -U root fournisseur_db

reservationdb:
	docker exec -it reservation_db psql -U root reservation_db

start-client:
	docker-compose up build_client
	docker-compose up client

start-reservation:
	docker-compose up build_reservation
	docker-compose up reservation reservation_db

start-fournisseur:
	docker-compose up build_fournisseur
	docker-compose up fournisseur fournisseur_db