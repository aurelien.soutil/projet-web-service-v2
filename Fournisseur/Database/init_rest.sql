-- Création de toutes les tables pour le fournisseur

CREATE TABLE REST_STATION (
    StationId SERIAL PRIMARY KEY,
    StationNom VARCHAR(50) NOT NULL
);

CREATE TABLE REST_TRAJET (
    TrajetId SERIAL PRIMARY KEY,
    TrajetDateDepart TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    TrajetDateArrivee TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    StationDepartId INTEGER NOT NULL,
    StationArriveeId INTEGER NOT NULL,
    FOREIGN KEY (StationDepartId) REFERENCES REST_STATION (StationId),
    FOREIGN KEY (StationArriveeId) REFERENCES REST_STATION (StationId)
);

CREATE TABLE REST_TRAJET_PLACE (
    PlaceId SERIAL PRIMARY KEY,
    TrajetId INTEGER NOT NULL,
    PlaceNombre INTEGER NOT NULL DEFAULT 0,
    PlaceClasse VARCHAR(8) NOT NULL CHECK (PlaceClasse IN ('First', 'Business', 'Standart')) DEFAULT 'Standart',
    FOREIGN KEY (TrajetId) REFERENCES REST_TRAJET (TrajetId)
);

CREATE TABLE REST_RESERVATION (
    ResaId SERIAL PRIMARY KEY,
    TrajetId INTEGER NOT NULL,
    ResaNomPassager VARCHAR(50) NOT NULL DEFAULT '',
    ResaPrenomPassager VARCHAR(50) NOT NULL DEFAULT '',
    ResaClasse VARCHAR(8) NOT NULL CHECK (ResaClasse IN ('First', 'Business', 'Standart')) DEFAULT 'Standart',
    FOREIGN KEY (TrajetId) REFERENCES REST_TRAJET (TrajetId)
);

INSERT INTO REST_STATION (StationNom) VALUES
    ('Paris'),
    ('Lyon'),
    ('Marseille'),
    ('Bordeaux');

INSERT INTO REST_TRAJET (TrajetDateDepart, TrajetDateArrivee, StationDepartId, StationArriveeId) VALUES
    ('2023-08-10 08:00:00', '2023-08-10 10:30:00', 1, 2),
    ('2023-08-12 12:30:00', '2023-08-12 15:45:00', 3, 4),
    ('2023-08-14 09:15:00', '2023-08-14 11:45:00', 2, 1);

INSERT INTO REST_TRAJET_PLACE (TrajetId, PlaceNombre, PlaceClasse) VALUES
    (1, 50, 'First'),
    (1, 100, 'Business'),
    (1, 200, 'Standart'),
    (2, 30, 'First'),
    (2, 80, 'Business'),
    (2, 150, 'Standart'),
    (3, 40, 'First'),
    (3, 90, 'Business'),
    (3, 180, 'Standart');
