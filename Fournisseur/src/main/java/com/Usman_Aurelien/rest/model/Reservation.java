package com.Usman_Aurelien.rest.model;

import javax.persistence.*;

@Entity
@Table(name = "rest_reservation")
public class Reservation {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "resaid")
    private int resaId;
    
    @Column(name = "trajetid", nullable = false)
    private int trajetId;
    
    @Column(name = "resanompassager", nullable = false)
    private String resaNomPassager;
    
    @Column(name = "resaprenompassager", nullable = false)
    private String resaPrenomPassager;
    
    @Column(name = "resaclasse", nullable = false)
    private String resaClasse;

    public Reservation() {
    }

    public Reservation(int trajetId, String resaNomPassager, String resaPrenomPassager, String resaClasse) {
        this.trajetId = trajetId;
        this.resaNomPassager = resaNomPassager;
        this.resaPrenomPassager = resaPrenomPassager;
        this.resaClasse = resaClasse;
    }

    public int getResaId() {
        return resaId;
    }

    public int getTrajetId() {
        return trajetId;
    }

    public void setTrajetId(int trajetId) {
        this.trajetId = trajetId;
    }

    public String getResaNomPassager() {
        return resaNomPassager;
    }

    public void setResaNomPassager(String resaNomPassager) {
        this.resaNomPassager = resaNomPassager;
    }

    public String getResaPrenomPassager() {
        return resaPrenomPassager;
    }

    public void setResaPrenomPassager(String resaPrenomPassager) {
        this.resaPrenomPassager = resaPrenomPassager;
    }

    public String getResaClasse() {
        return resaClasse;
    }

    public void setResaClasse(String resaClasse) {
        this.resaClasse = resaClasse;
    }
}
