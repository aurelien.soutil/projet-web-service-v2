package com.Usman_Aurelien.rest.model;

import javax.persistence.*;

@Entity
@Table(name = "rest_trajet_place")
public class TrajetPlace {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "placeid")
    private int placeId;
    
    @Column(name = "trajetid", nullable = false)
    private int trajetId;
    
    @Column(name = "placenombre", nullable = false)
    private int placeNombre;
    
    @Column(name = "placeclasse", nullable = false)
    private String placeClasse;

    public TrajetPlace() {
    }

    public TrajetPlace(int trajetId, int placeNombre, String placeClasse) {
        this.trajetId = trajetId;
        this.placeNombre = placeNombre;
        this.placeClasse = placeClasse;
    }

    public int getPlaceId() {
        return placeId;
    }

    public int getTrajetId() {
        return trajetId;
    }

    public void setTrajetId(int trajetId) {
        this.trajetId = trajetId;
    }

    public int getPlaceNombre() {
        return placeNombre;
    }

    public void setPlaceNombre(int placeNombre) {
        this.placeNombre = placeNombre;
    }

    public String getPlaceClasse() {
        return placeClasse;
    }

    public void setPlaceClasse(String placeClasse) {
        this.placeClasse = placeClasse;
    }
}
