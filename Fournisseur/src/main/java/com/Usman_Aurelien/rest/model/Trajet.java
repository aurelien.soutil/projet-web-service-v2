package com.Usman_Aurelien.rest.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;

@Entity
@Table(name = "rest_trajet")
public class Trajet {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trajetid")
    private int trajetId;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "trajetdatedepart", nullable = false)
    private Timestamp trajetDateDepart;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "trajetdatearrivee", nullable = false)
    private Timestamp trajetDateArrivee;
    
    @Column(name = "stationdepartid", nullable = false)
    private int stationDepartId;
    
    @Column(name = "stationarriveeid", nullable = false)
    private int stationArriveeId;

    public Trajet() {
    }

    public Trajet(Timestamp trajetDateDepart, Timestamp trajetDateArrivee, int stationDepartId, int stationArriveeId) {
        this.trajetDateDepart = trajetDateDepart;
        this.trajetDateArrivee = trajetDateArrivee;
        this.stationDepartId = stationDepartId;
        this.stationArriveeId = stationArriveeId;
    }

    public int getTrajetId() {
        return trajetId;
    }

    public Timestamp getTrajetDateDepart() {
        return trajetDateDepart;
    }

    public void setTrajetDateDepart(Timestamp trajetDateDepart) {
        this.trajetDateDepart = trajetDateDepart;
    }

    public Timestamp getTrajetDateArrivee() {
        return trajetDateArrivee;
    }

    public void setTrajetDateArrivee(Timestamp trajetDateArrivee) {
        this.trajetDateArrivee = trajetDateArrivee;
    }

    public int getStationDepartId() {
        return stationDepartId;
    }

    public void setStationDepartId(int stationDepartId) {
        this.stationDepartId = stationDepartId;
    }

    public int getStationArriveeId() {
        return stationArriveeId;
    }

    public void setStationArriveeId(int stationArriveeId) {
        this.stationArriveeId = stationArriveeId;
    }
}
