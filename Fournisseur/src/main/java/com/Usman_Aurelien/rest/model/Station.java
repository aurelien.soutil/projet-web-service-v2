package com.Usman_Aurelien.rest.model;

import javax.persistence.*;

@Entity
@Table(name = "rest_station")
public class Station {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stationid")
    private int stationId;

    @Column(name = "stationnom", nullable = false)
    private String stationNom;

    public Station() {
    }

    public Station(String stationNom) {
        this.stationNom = stationNom;
    }

    public int getStationId() {
        return stationId;
    }

    public String getStationNom() {
        return stationNom;
    }

    public void setStationNom(String stationNom) {
        this.stationNom = stationNom;
    } 

}
