package com.Usman_Aurelien.rest.config;

import io.swagger.jaxrs.config.BeanConfig;

public class SwaggerConfig {

    public SwaggerConfig() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.0");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("host.docker.internal:8081");
        beanConfig.setBasePath("/api");
        beanConfig.setResourcePackage("com.Usman_Aurelien.rest.controller");
        beanConfig.setScan(true);
    }
}
