package com.Usman_Aurelien.rest.controller;

import com.Usman_Aurelien.rest.model.Reservation;
import com.Usman_Aurelien.rest.config.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/reservations")
public class ReservationController {

    private SessionFactory sessionFactory;

    public ReservationController() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Reservation> getAllReservations() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Reservation", Reservation.class).list();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Reservation getReservation(@PathParam("id") int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Reservation.class, id);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addReservation(Reservation reservation) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(reservation);
            tx.commit();
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateReservation(@PathParam("id") int id, Reservation reservation) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Reservation existingReservation = session.get(Reservation.class, id);
            existingReservation.setTrajetId(reservation.getTrajetId());
            existingReservation.setResaNomPassager(reservation.getResaNomPassager());
            existingReservation.setResaPrenomPassager(reservation.getResaPrenomPassager());
            existingReservation.setResaClasse(reservation.getResaClasse());
            session.update(existingReservation);
            tx.commit();
        }
    }

    @DELETE
    @Path("/{id}")
    public void deleteReservation(@PathParam("id") int id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Reservation reservation = session.get(Reservation.class, id);
            if (reservation != null) {
                session.delete(reservation);
            }
            tx.commit();
        }
    }
}
