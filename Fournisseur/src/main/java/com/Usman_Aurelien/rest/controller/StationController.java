package com.Usman_Aurelien.rest.controller;

import com.Usman_Aurelien.rest.model.Station;
import com.Usman_Aurelien.rest.config.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/stations")
public class StationController {

    private SessionFactory sessionFactory;

    public StationController() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Station> getAllStations() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Station", Station.class).list();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addStation(Station station) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(station);
            tx.commit();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Station getStation(@PathParam("id") int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Station.class, id);
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateStation(@PathParam("id") int id, Station station) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Station existingStation = session.get(Station.class, id);
            if (existingStation != null) {
                existingStation.setStationNom(station.getStationNom());
                session.update(existingStation);
            }
            tx.commit();
        }
    }

    @DELETE
    @Path("/{id}")
    public void deleteStation(@PathParam("id") int id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Station station = session.get(Station.class, id);
            if (station != null) {
                session.delete(station);
            }
            tx.commit();
        }
    }
}
