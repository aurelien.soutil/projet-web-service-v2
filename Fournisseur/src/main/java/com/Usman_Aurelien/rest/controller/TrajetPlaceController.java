package com.Usman_Aurelien.rest.controller;

import com.Usman_Aurelien.rest.model.TrajetPlace;
import com.Usman_Aurelien.rest.config.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/trajetPlaces")
public class TrajetPlaceController {

    private SessionFactory sessionFactory;

    public TrajetPlaceController() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TrajetPlace> getAllTrajetPlaces() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from TrajetPlace", TrajetPlace.class).list();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public TrajetPlace getTrajetPlace(@PathParam("id") int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(TrajetPlace.class, id);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addTrajetPlace(TrajetPlace trajetPlace) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(trajetPlace);
            tx.commit();
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateTrajetPlace(@PathParam("id") int id, TrajetPlace trajetPlace) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            TrajetPlace existingTrajetPlace = session.get(TrajetPlace.class, id);
            if (existingTrajetPlace != null) {
                existingTrajetPlace.setTrajetId(trajetPlace.getTrajetId());
                existingTrajetPlace.setPlaceNombre(trajetPlace.getPlaceNombre());
                existingTrajetPlace.setPlaceClasse(trajetPlace.getPlaceClasse());
                session.update(existingTrajetPlace);
            }
            tx.commit();
        }
    }

    @DELETE
    @Path("/{id}")
    public void deleteTrajetPlace(@PathParam("id") int id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            TrajetPlace trajetPlace = session.get(TrajetPlace.class, id);
            if (trajetPlace != null) {
                session.delete(trajetPlace);
            }
            tx.commit();
        }
    }
}
