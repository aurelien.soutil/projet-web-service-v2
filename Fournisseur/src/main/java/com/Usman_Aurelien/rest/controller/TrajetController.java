package com.Usman_Aurelien.rest.controller;

import com.Usman_Aurelien.rest.model.Trajet;
import com.Usman_Aurelien.rest.config.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/trajets")
public class TrajetController {

    private SessionFactory sessionFactory;

    public TrajetController() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Trajet> getAllTrajets() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Trajet", Trajet.class).list();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Trajet getTrajet(@PathParam("id") int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Trajet.class, id);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addTrajet(Trajet trajet) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(trajet);
            tx.commit();
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateTrajet(@PathParam("id") int id, Trajet trajet) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Trajet existingTrajet = session.get(Trajet.class, id);
            if (existingTrajet != null) {
                existingTrajet.setTrajetDateDepart(trajet.getTrajetDateDepart());
                existingTrajet.setTrajetDateArrivee(trajet.getTrajetDateArrivee());
                existingTrajet.setStationDepartId(trajet.getStationDepartId());
                existingTrajet.setStationArriveeId(trajet.getStationArriveeId());
                session.update(existingTrajet);
            }
            tx.commit();
        }
    }

    @DELETE
    @Path("/{id}")
    public void deleteTrajet(@PathParam("id") int id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            Trajet trajet = session.get(Trajet.class, id);
            if (trajet != null) {
                session.delete(trajet);
            }
            tx.commit();
        }
    }
}
