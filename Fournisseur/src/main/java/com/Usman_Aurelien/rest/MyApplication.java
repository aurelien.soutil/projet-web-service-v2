package com.Usman_Aurelien.rest;

import com.Usman_Aurelien.rest.config.SwaggerConfig;
import com.Usman_Aurelien.rest.controller.*;
import com.Usman_Aurelien.rest.config.CorsFilter;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/api")
public class MyApplication extends ResourceConfig {
    public MyApplication() {
        register(StationController.class);
        register(TrajetController.class);
        register(TrajetPlaceController.class);
        register(ReservationController.class);

        register(CorsFilter.class);

        packages("com.Usman_Aurelien.rest.controller");

        new SwaggerConfig();

        register(io.swagger.jaxrs.listing.ApiListingResource.class);
        register(io.swagger.jaxrs.listing.SwaggerSerializers.class);
    }
}
