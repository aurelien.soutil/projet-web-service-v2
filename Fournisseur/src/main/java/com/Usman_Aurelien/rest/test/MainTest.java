package com.Usman_Aurelien.rest.test;

import com.Usman_Aurelien.rest.controller.StationController;
import com.Usman_Aurelien.rest.model.Station;

public class MainTest {

    public static void main(String[] args) {
        // Créer une nouvelle station
        Station nouvelleStation = new Station();
        nouvelleStation.setStationNom("Gare d'Ermont-Eaubonne");

        // Créer une instance de StationController
        StationController stationController = new StationController();

        // Appeler la méthode addStation
        stationController.addStation(nouvelleStation);

        System.out.println("Station ajoutée via StationController");

        // Appeler la méthode getAllStations
        stationController.getAllStations().forEach(station -> System.out.println(station.getStationNom()));
    }
}
